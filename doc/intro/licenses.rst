.. _licenses:

Licenses
========

This chapter lists the licences disclaimers of the individual software components, which are part of the *openmatDB*
environment.
For details on the license of the individual components see also the LICENSE and NOTICE files inside the corresponding
source code repository:

* https://gitlab.com/thecker/pyopenmatdb
* https://gitlab.com/thecker/openmat-plugins
* https://gitlab.com/thecker/openmat-webui
* https://gitlab.com/thecker/openmatGUI
* https://gitlab.com/thecker/openmat-testenv

pyopenmatdb
-----------

.. code-block:: none

    Copyright 2020 Thies Hecker

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

openmat-plugins
---------------

.. code-block:: none

    Copyright 2020 Thies Hecker

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

openmat-webui
---------------

.. code-block:: none

    openmat-webui - a browser based GUI for the openmatDB environment

        Copyright 2020 Thies Hecker

        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.


    3rd party Libraries bundled with openmat-webui
    ==============================================

    This project comes bundled with following 3rd party libraries:

    * jQuery JavaScript Library v1.12.4
    * jQuery UI v1.12.1
    * jQuery UI-themes v1.12.1
    * Fancytree v2.34.0

    The copyright notices and license disclaimers can be found below.

    jQuery JavaScript Library v1.12.4
    ---------------------------------

        jQuery JavaScript Library v1.12.4
        http://jquery.com/

        Includes Sizzle.js
        http://sizzlejs.com/

        Copyright jQuery Foundation and other contributors
        Released under the MIT license
        http://jquery.org/license

    In openmat-webui the re-distributed file (including the license information)
    of jQuery is located in:
    /webpage/static/jquery-ui/external/jquery/jquery.js


    jQuery UI v1.12.1
    -----------------

        Copyright jQuery Foundation and other contributors, https://jquery.org/

        This software consists of voluntary contributions made by many
        individuals. For exact contribution history, see the revision history
        available at https://github.com/jquery/jquery-ui

        The following license applies to all parts of this software except as
        documented below:

        ====

        Permission is hereby granted, free of charge, to any person obtaining
        a copy of this software and associated documentation files (the
        "Software"), to deal in the Software without restriction, including
        without limitation the rights to use, copy, modify, merge, publish,
        distribute, sublicense, and/or sell copies of the Software, and to
        permit persons to whom the Software is furnished to do so, subject to
        the following conditions:

        The above copyright notice and this permission notice shall be
        included in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
        EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
        NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
        LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
        OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
        WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

        ====

        Copyright and related rights for sample code are waived via CC0. Sample
        code is defined as all source code contained within the demos directory.

        CC0: http://creativecommons.org/publicdomain/zero/1.0/

        ====

        All files located in the node_modules and external directories are
        externally maintained libraries used by this software which have their
        own licenses; we recommend you read them, as their terms may differ from
        the terms above.

    In the openmat-webui repository the re-distributed files (including the
    original license file) of jQuery UI are located under:
    /webpage/static/jquery-ui/


    jQuery UI-themes v1.12.1
    ------------------------

        Copyright jQuery Foundation and other contributors, https://jquery.org/

        This software consists of voluntary contributions made by many
        individuals. For exact contribution history, see the revision history
        available at https://github.com/jquery/jquery-ui

        The following license applies to all parts of this software except as
        documented below:

        ====

        Permission is hereby granted, free of charge, to any person obtaining
        a copy of this software and associated documentation files (the
        "Software"), to deal in the Software without restriction, including
        without limitation the rights to use, copy, modify, merge, publish,
        distribute, sublicense, and/or sell copies of the Software, and to
        permit persons to whom the Software is furnished to do so, subject to
        the following conditions:

        The above copyright notice and this permission notice shall be
        included in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
        EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
        NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
        LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
        OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
        WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

        ====

        Copyright and related rights for sample code are waived via CC0. Sample
        code is defined as all source code contained within the demos directory.

        CC0: http://creativecommons.org/publicdomain/zero/1.0/

        ====

        All files located in the node_modules and external directories are
        externally maintained libraries used by this software which have their
        own licenses; we recommend you read them, as their terms may differ from
        the terms above.

    In the openmat-webui repository the re-distributed files (including the
    original license file) of jQuery UI-themes are located under:
    /webpage/static/jquery-ui-themes/


    Fancytree v2.34.0
    -----------------

        Copyright 2008-2019 Martin Wendt,
        https://wwWendt.de/

        Permission is hereby granted, free of charge, to any person obtaining
        a copy of this software and associated documentation files (the
        "Software"), to deal in the Software without restriction, including
        without limitation the rights to use, copy, modify, merge, publish,
        distribute, sublicense, and/or sell copies of the Software, and to
        permit persons to whom the Software is furnished to do so, subject to
        the following conditions:

        The above copyright notice and this permission notice shall be
        included in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
        EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
        NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
        LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
        OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
        WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    In the openmat-webui repository the re-distributed files (including the
    original license file) of Fancytree are located under:
    /webpage/node_modules/jquery.fancytree/


openmatGUI
----------

.. code-block:: none

    openmatGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with openmatGUI.  If not, see <https://www.gnu.org/licenses/>.

openmat-testenv
---------------

.. code-block:: none

    Copyright 2020 Thies Hecker

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
