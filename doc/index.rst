.. openmatDB documentation master file, created by
   sphinx-quickstart on Thu Jan 23 19:20:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to openmatDB's documentation!
=====================================

*openmatDB* (open Modular and exTendable Data Base) is a free (see :ref:`licenses`) data base framework primarily aimed
at scientific/engineering data base applications with a focus on easy extendability through user plug-ins (e.g. for
specific and complex data analysis).

By using a plug-in concept which relies on RESTful APIs and JSON as an exchange format, *openmatDB* can easily be
extended to support plug-ins written in different languages or plug-ins distributed on multiple remote servers.

Since even basic data base functions are provided by plug-ins the code base of the core modules is kept small and
simple.

For more details on the *openmatDB* concept and its features see the :ref:`introduction`.

*openmatDB* is currently mainly written in *python* and *JavaScript* (for the web-interface) and its components have
a number dependencies on 3rd party libraries - see :ref:`components` for details.

For details on the licences of the *openmatDB* components see :ref:`licenses`.

.. note::

   *openmatDB* is currently in its early development stages and not suitable for a production system. However A complete
   test environment (including a desktop and a browser based GUI) can however be downloaded - see :ref:`test_env`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro/intro
   intro/components
   intro/licenses
   plugins/plugins
   test_env/test_env
   api/api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
