This file is part of the pyopenmatdb project.

Copyright 2020 Thies Hecker

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Additional remarks
==================

pyopenmatdb imports a couple modules/classes/functions from third party
libraries/packages (i.e. "dependencies"). In order to use pyopenmatdb in its
intended scope of functionality you will need to install theses dependencies
and make sure that your application complies with their licenses as well.

Dependencies of pyopenmatdb include (version numbers serve as a reference
for copyright information and hyperlinks only):

Requests (v2.22.0), Copyright 2018 Kenneth Reitz,
see https://requests.readthedocs.io/en/master/

Flask (v1.1.1), Copyright 2010 Pallets,
see https://www.palletsprojects.com/p/flask/

Flask-CORS (v.3.0.8), Copyright (C) 2016 Cory Dolphin, Olin College,
see https://flask-cors.readthedocs.io/en/latest/

various modules/packages of the Python Standard Library (v3.8.1),
Copyright © 2001-2020 Python Software Foundation; All Rights Reserved,
https://docs.python.org/3/library/

Notes:
* The copyright information and hyperlinks were taken from the version given in
  parenthesis. They may differ, if you choose another version of the package.
  Make sure to check the license requirements of exactly the version, that you
  are using in your application.
* The packages above most likely have further dependencies, having different
  copyrights and licenses.