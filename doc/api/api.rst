.. _apiRef:

API reference
=============

This section describes the API of the *pyserver* and *pyopenmatdb* module.

.. _api_classes_for_plugins:

API for creating plug-ins in python
-----------------------------------

Each plug-in's *run*-function shall have *session* as its first argument. *session* is an instance of
:ref:`api_plugin_server`.
It has an attribute *db_client* which holds an instance :ref:`api_CouchClient` - i.e. providing direct data base
functions to the plug-ins.

.. _api_plugin_server:

PythonPluginSever
~~~~~~~~~~~~~~~~~

.. autoclass:: openmatdb.pyserver.PythonPluginServer
    :members:

.. _api_CouchClient:

CouchClient
~~~~~~~~~~~

.. autoclass:: openmatdb.pyopenmatdb.CouchClient
    :members:

API for creating UIs in python
------------------------------

Each UI needs to communicate with the plug-in servers. The *OpenmatEnvironment* class provides an interface for this.

OpenmatEnvironemnt
~~~~~~~~~~~~~~~~~~

.. autoclass:: openmatdb.pyopenmatdb.OpenmatEnvironment
    :members:


RESTful API for building plugin-servers
---------------------------------------

TBD
