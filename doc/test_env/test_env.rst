.. _test_env:

Test environment
================

.. warning::

    The test environment will run some servers on docker_ containers on your local machine. In order to allow
    communication between the desktop-UI and the plug-in servers or to access the web-UI via a browser some ports of
    the containers are exposed to the host system.
    Read the `Docker security`_ section on the docker website for details on security with regard to docker containers.

    The test environment is not intended for production environments - it should only be used for testing and
    development!

Prerequisites
-------------

*openmatDB* is developed and tested under GNU/linux. Since most of the code is based on interpreted languages it should
be possible to run on other platforms (e.g. MS Windows) as well, but for this chapter we assume you are running
a linux machine.

For building the test environment following programs have to installed on your system:

* docker
* cURL
* python3

Every major linux distribution should have official packages for these tools.

Obtain the source code
----------------------

In order to setup a full test environment you will first have to download/clone the source code of all *openmatDB*
repositories - see :ref:`components`.

Assuming you want to store the data for the *openmatDB* environment in a folder called "openmatdb" in your home
directory, open a terminal and run following commands:

.. code-block:: none

    cd ~
    mkdir openmatdb
    cd openmatdb
    git clone https://gitlab.com/thecker/pyopenmatdb.git
    git clone https://gitlab.com/thecker/openmat-plugins.git
    git clone https://gitlab.com/thecker/openmat-webui.git
    git clone https://gitlab.com/thecker/openmatGUI.git
    git clone https://gitlab.com/thecker/openmat-testenv.git

Build docker images
-------------------

In order to build the docker images required to run the containers change into the *openmat-testenv* folder and start
the setup script:

.. code-block:: none

    cd openmat-testenv
    ./setup_env.sh

This shell script will pull pre-built docker images for *couchDB* and a *python* environment. It build images for the
*openmatDB* plug-in server and the web-ui (openmat-py_plguin:latest and openmat-webui:latest) based on the *python*
image.

To build the *openmatDB* images the python packages (and their dependencies) required by *pyopenmatdb*,
*openmat-plugins* and *openmat-webui* will be downloaded and installed on the images as well.

Check if the docker network 'openmat-net' exists and create it if not (the containers will communicate inside this
network and only expose dedicated ports to the host system)

Start docker containers
-----------------------

After you have build the docker images, you can start the containers (make sure the docker daemon is running).
A shell script to start the containers with the corresponding configuration is provided.

.. code-block:: none

    ./start_all.sh

This script will do following steps:

* start the *couchDB* instance - container name: "couch_test"
* get the IP address of *couchDB* inside the docker network and update plug-in server config files
* start the first plug-in server - container name: "openmat_py_plugins"
* start the second plug-in server - container name: "openmat_py_plugins2"
* get the IP addresses of the plug-in servers in the docker network and update web-interface config files
* start the server for the web-interface (in interactive mode) - container name: "openmat_webui"

You should now see the log of the web-interface (e.g. incoming HTTP requests) in the terminal.

If you open up a browser window at http://0.0.0.0:5010 you should now get access to the *openmatDB* web-interface.
However the data base is still empty.

.. note::

    In order to easily modify the programs running on the containers, the folders of the cloned *openmatDB* repositories
    are mapped to the containers internal file systems. I.e. all changes made to the repository folders will be
    reflected in the containers.

Importing test data
-------------------

The easiest way to import test data is to go to the web-interface and in the "plug-ins" tab select the "Import records
from Excel" plug-in. The parameters will default to an excel file which is mapped to the filesystem of the
plug-in server (you can find it in the "py_plugins1_conf/files" sub directory inside your "openmat-testenv" folder).
So you can just click the "Run plug-in" button. You should see a list of record names, which have been imported from the
excel file.

.. image:: import_data.png

Connecting with the desktop-GUI
-------------------------------

You can simultaneously connect with the desktop-GUI. Before starting the desktop-GUI you should do 2 steps:

First you should make sure, that the required python packages to run the desktop-GUI are installed.:

* requests
* PyQt5
* matplotlib

You can install these either via your linux distributions packages, *pip* or *conda*,... E.g. via *pip* just open up a
new terminal type:

.. code-block:: none

    pip install requests PyQt5 matplotlib

Next you need to add the python modules in the *pyopenmatdb* folder to your PYTHONPATH:

.. code-block:: none

    cd ~/openmatdb/pyopenmatdb
    export PYTHONPATH=$PYTHONPATH:$(pwd)

Afterwards you can start the desktop-GUI:

.. code-block:: none

    cd ../openmatGUI
    python gui.py

.. note::

    Packages for *pip* and *conda* will be provided for future releases, which will take care of the dependency issues
    and the steps above.

Stopping the test environment
-----------------------------

To stop the containers just press CTRL+C in the terminal. You should see a message "All containers stopped successfully"
in the end.

Additional remarks
------------------

Note that you can also use *Fauxton* (an administration tool that comes with *couchDB*) to access the
data base. To do that open your browser at http://127.0.0.1:5984/_utils/



.. _docker: https://docs.docker.com/
.. _Docker security: https://docs.docker.com/engine/security/security/

