# pyserver module - contains the functions and classes for the plug-in server
#
# This file is part of the pyopenmatdb project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import requests
import warnings
import sys
import os
from flask import Flask, request
from flask_cors import CORS
import json
from os import listdir
from os.path import isfile, join

from openmatdb.pyopenmatdb import CouchClient


def register_plugins(plugin_dir):
    """Registers the plugins by calling their register function"""

    abs_plugin_dir = os.path.abspath(plugin_dir)
    print(abs_plugin_dir)
    sys.path.append(abs_plugin_dir)

    # get only files and only with extension .py
    plugin_files = [f for f in listdir(plugin_dir) if isfile(join(plugin_dir, f)) and f[-3:] == '.py']

    plugin_config = []
    for plugin_file in plugin_files:
        plugin_name = plugin_file.replace('.py', '')
        print(plugin_name)
        #TODO: check if there is a better way to run the plugins register function
        ldict = {}
        exec('from {} import register'.format(plugin_name), globals(), ldict)
        plugin_dict = ldict['register']()    # this function needs to be in every plug-in
        plugin_dict['module'] = plugin_name
        plugin_dict['filename'] = plugin_file
        plugin_config.append(plugin_dict)

    print(plugin_config)

    return plugin_config


class PythonPluginServer(Flask):

    def __init__(self, __name__):

        """Server for python plug-ins

        Attributes:
            name(str): Name of the server
            plugin_language(str): Programming language of the plug-ins on this server
            plugin_dir(str): Folder on the server, where the plug-ins are located
            db_client(CouchClient): The interface object for the data base
            plugin_config(list): A list of dictionaries, which store the information for each plug-in on the server
            plugin_route_table(dict): A routing table - dict with plugin-name as key and server address as value
        """

        super().__init__(__name__)
        self.plugin_config = None
        self.db_client = None
        self.name = None
        self.plugin_language = 'python'
        self.plugin_route_table = None
        self.plugin_dir = None

    @staticmethod
    def _decode(request):
        """Decodes a plug-in run-request (reads the JSON data in the body)

        Args:
            request(request): Flask request object

        Returns:
            tuple: plugin_name (str), plugin_params (list)

        Note:
            The JSON data in the body of a plug-in run request always needs to consist of two keys:
                * 'name' - name of the plug-in to launch
                * 'params' - parameters to pass the plug-ins run function
        """
        data_str = request.get_data(as_text=True)
        data = json.loads(data_str)
        plugin_name = data['name']  # name of plugin
        params = data['params']  # list of plugin param values

        return plugin_name, params

    @staticmethod
    def _encode(plugin_name, params):
        """Encodes a plug-in run-request"""
        plugin_data = {'name': plugin_name, 'params': params}
        return json.dumps(plugin_data)

    def _run_local_plugin(self, plugin_name, *params):
        """Launches a plug-in, which installed on the server"""
        # TODO: check if there is a better way to run the plug-in's run function
        ldict = {}
        exec('from {} import run'.format(plugin_name), globals(), ldict)
        try:
            if params in [None, [], ()]:
                result = ldict['run'](self)
            else:
                result = ldict['run'](self, *params)  # the 'run' function needs to be in every plug-in
        except Exception as Err:
            warnings.warn("Plug-in \"{}\" crashed with error type {}. \"{}\". Returning \"None\".".format(plugin_name,
                                                                                                          type(Err),
                                                                                                          Err))
            result = None

        return result

    def _run_remote_plugin(self, plugin_server_url, plugin_name, params):
        """Requests to run a plug-in on a remote plug-in server"""

        plugin_data = self._encode(plugin_name, params)

        headers = {'Content-Type': 'application/json'}
        url = plugin_server_url + '/run_plugin'
        resp = requests.post(url, plugin_data, headers=headers)
        return json.loads(resp.text)

    def run_plugin(self, plugin_name, *params):
        """Request to run a plug-in (from inside a plug-in)

        Args:
            plugin_name(str): name of the plug-in
            params: Parameters to pass to the plug-in's run function as arguments
        """

        # check if this is a local plug-in
        local_plugins = [plugin['module'] for plugin in self.plugin_config]
        if plugin_name in local_plugins:
            resp = self._run_local_plugin(plugin_name, *params)
        else:  # the plug-in is not available on the local plug-in server
            # check if the plug-in can be found in the route-table
            if plugin_name in app.plugin_route_table.keys():
                plugin_server_url = app.plugin_route_table[plugin_name]
                resp = self._run_remote_plugin(plugin_server_url, plugin_name, params)

            else:
                warnings.warn('Plug-in {} not in plug-in route table!')
                return None
        return resp

    def update_plugin_registry(self):
        """Updates the plugin_config attribute"""

        if self.plugin_dir:
            self.plugin_config = register_plugins(self.plugin_dir)
        else:
            warnings.warn('No plug-in dir defined. Cannot update registry.')


app = PythonPluginServer(__name__)
CORS(app)


# these are HTTP requests exposed by flask
@app.route("/server_info", methods=['GET'])
def show_info():
    """Returns information about the server"""
    return json.dumps({'server_name': app.name, 'plugin_language': app.plugin_language})


@app.route("/show_plugins", methods=['GET'])
def show_plugins():
    """Returns the available plugins"""
    plugin_config = app.plugin_config
    return json.dumps(plugin_config)


@app.route("/update_plugin_route_table", methods=['POST'])
def update_plugin_route_table():
    """Updates the plug-in route table (i.e. dict with plug-in names on all plug-in servers in the system)"""
    data_str = request.get_data(as_text=True)
    app.plugin_route_table = json.loads(data_str)
    return json.dumps('route table updated')


@app.route("/run_plugin", methods=['POST'])
def run_plugin_request():
    """Process a request to run a plugin on this server (with plug-in name and params defined in the JSON data)

    Note:
        Request body must be JSON data with keys 'name' (str) and 'params' (list of parameter values)
    """
    plugin_name, params = app._decode(request)

    result = app._run_local_plugin(plugin_name, *params)

    return json.dumps(result, default=str)


if __name__ == "__main__":
    # initialize plugins
    plugin_dir = "../plugins"
    app.plugin_dir = plugin_dir
    app.update_plugin_registry()

    # load some config from a file. Helpful for docker containers running the same python code, but mapped to different
    # config files folders.
    with open('../config/config.json', 'r') as file:
        config = json.loads(file.read())

    # TODO: This is the URL if the openmatDB web UI runs in a docker container,
    #  if run from localhost it will be 0.0.0.0:5000/v1
    # this is the address to teh RESTful API for couchDB
    api_url = config['db_url']
    db_client = CouchClient(api_url=api_url, db_name='test_db', record_id_fields=['last_name', 'first_name'])

    app.db_client = db_client
    app.name = config['server_name']

    app.run(config['address'], config['port'])
