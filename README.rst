About
=====

*pyopenmatdb* contains the python implementation of the core classes for the *openmatDB* environment.

It also includes the sphinx documentation sources (see ./doc folder)

*pyopenmatdb* is free software released under the Apache License 2.0 - see NOTICE and LICENSE files for details.

*pyopenmatdb* is written in python and depends on following packages:

* python3
* Requests
* Flask
* Flask-CORS

For further information on *openmatDB* and its components see https://openmatdb.readthedocs.io/en/latest/