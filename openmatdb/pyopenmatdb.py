# pyopenmatdb module - contains core classes for DB clients and environment
#
# This file is part of the pyopenmatdb project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import urllib
import requests
import warnings
import json
import binascii
import os


def dict2percent(input_dict):
    """Converts a dictionary to a string in percent notation"""
    return urllib.parse.quote(str(input_dict).replace('\'', '\"'), safe="")


def _convert_response(resp):
    """Converts the request response into a dictionary for the body"""
    if resp.status_code != 200:
        warnings.warn("Could not convert data - status code: {}".format(resp.status_code))
        return {}
    else:
        return json.loads(resp.text)


class CouchClient:

    def __init__(self, api_url, db_name, record_id_fields):

        """Client for the RESTful API of couchDB

        Args:
            api_url(str): Url to data base (incl. port number)
            db_name(str): Data base name
            record_id_fields(list): List of str - record attribute names to join to record Ids.

        Attributes:
            api_url(str): Url to data base (incl. port number)
            db_name(str): Data base name
            record_id_fields(list): List of str - record attribute names to join to record Ids.
        """

        self.api_url = api_url
        self.db_name = db_name
        self.record_id_fields = record_id_fields

    def _query_records(self, collection, query_dict=None):
        """Sent HTTP GET request with a filter defined by a dict to query the data base"""
        headers = {'Content-Type': 'application/json'}
        # update the query dict for the collection
        if query_dict:
            query_dict['collection'] = collection
        else:
            query_dict = {'collection': collection}

        find_dict = {
            "selector": query_dict,
        }

        # url = self.api_url + "/" + self.db_name + "/_design/" + collection + "/_view/all"
        # resp = requests.get(url=url)

        url = self.api_url + "/" + self.db_name + "/_find"
        resp = requests.post(url, json.dumps(find_dict), headers=headers)

        return resp

    def _query_record_by_ID(self, record_ID):
        """Gets a single record by its _id or alias"""
        url = self.api_url + '/' + self.db_name + '/' + record_ID
        resp = requests.get(url=url)
        return resp

    def _put_record(self, record_id, record_data):
        """Posts a record to the data base"""

        headers = {'Content-Type': 'application/json'}
        url = self.api_url + '/' + self.db_name + '/' + record_id
        # record_str = dict2percent(record_data)
        resp = requests.put(url, json.dumps(record_data), headers=headers)
        return resp

    def _post_records(self, collection, record_list):
        """Posts records to the data base

        Args:
            collection(str): Name of the collection
            record_list(list): List of dictionaries with record attributes and values
        """
        headers = {'Content-Type': 'application/json'}
        url = self.api_url + '/' + collection
        resp = requests.post(url, json.dumps(record_list), headers=headers)
        return resp

    def _query_collections(self):
        """Get collections"""

        url = self.api_url + "/" + self.db_name + "/_all_docs?startkey=\"_design\"&endkey=\"_design0\""
        resp = requests.get(url=url)
        return resp

    def _put_collection(self, collection_name):
        """Put a new view/collection into the database"""
        headers = {'Content-Type': 'application/json'}

        id = "_design/{}".format(collection_name)
        url = self.api_url + "/" + self.db_name + "/" + id
        map_func = "function(doc){{ if (doc.collection && doc.collection == \'{}\') " \
                   "{{ emit(doc._id, doc._rev)}}}}".format(collection_name)

        collection_view = {
            "_id": id,
            "views": {
                "all": {
                    "map": map_func
                }
            }
        }

        resp = requests.put(url, json.dumps(collection_view), headers=headers)
        return resp

    def get_collections(self):
        """Get collections"""
        resp = self._query_collections()
        conv_resp = _convert_response(resp)

        collections = [result['id'].replace('_design/', '') for result in conv_resp['rows']]
        return collections

    def add_collection(self, collection_name):
        """Adds a new view/collection"""
        resp = self._put_collection(collection_name)
        return resp.text

    def add_record(self, collection, record_data):
        """Adds a record to the data base"""
        # TODO: improve auto generating record ids

        record_data['collection'] = collection

        record_id = ""
        try:
            for id_key in self.record_id_fields:
                record_id = record_id + record_data[id_key]
        except KeyError:
            record_id = binascii.b2a_hex(os.urandom(15)).decode()

        resp = self._put_record(record_id=record_id, record_data=record_data)
        return resp

    def add_records(self, collection, record_list):
        """Add records to a collection

        Args:
            collection(str): Name of the collection
            record_list(list): List of dictionaries with record attributes and values

        Returns:
            list: List of valid records
        """
        valids = []
        for record in record_list:
            resp = self.add_record(collection=collection, record_data=record)
            if resp.status_code == 201:
                if resp.ok:
                    valids.append(json.loads(resp.text)['id'])

        return valids

    def get_records(self, collection, query_dict=None):
        """Returns records as a list"""
        resp = self._query_records(collection=collection, query_dict=query_dict)
        records_ = _convert_response(resp)

        records = records_['docs']

        return records

    def get_record_by_ID(self, collection, record_ID):
        """Get record by ID - collection"""
        resp = self._query_record_by_ID(record_ID)
        record = _convert_response(resp)
        return record


class OpenmatEnvironment:

    def __init__(self):
        """Manager for the openmatDB environment (mainly communication with plug-in servers)

        Attributes:
            plugin_servers(list): A list of server information. Dict for each server with keys:
                                  'server_name', 'url', 'plug-ins', 'plugin_language'
            active_record_id(str): ID of the currently active record (to be set by UI)
            active_collection(str): Name of the active collection (to be set by UI)

        Note:
            The active... attributes can be used to provide default values to plug-ins based on currently selected
            items in the UI.

        """
        self.plugin_servers = []
        self.active_record_id = None
        self.active_collection = None

    def _request_plugin_run(self, plugin_server_url, plugin_data):
        """Requests a plug-in run on the specified plug-in server"""
        headers = {'Content-Type': 'application/json'}
        url = plugin_server_url + '/run_plugin'
        resp = requests.post(url, json.dumps(plugin_data), headers=headers)
        return resp

    def _send_route_table(self, plugin_server_url):
        """Sends the current plug-in route table to a plug-in server"""
        headers = {'Content-Type': 'application/json'}
        url = plugin_server_url + '/update_plugin_route_table'
        resp = requests.post(url, json.dumps(self.plugin_route_table), headers=headers)
        return resp

    def get_plugin_server_data(self, url):
        """
        Retrieves information from a plug-in server (i.e. server name, plug-in language, available plug-ins + config)
        Args:
            url(str): URL to plug-in server API

        Returns:
            dict: with keys: 'server_name', 'url', 'plug-ins', 'plugin_language'
        """
        request_url = url + '/server_info'
        resp_info = requests.get(url=request_url)
        info = _convert_response(resp_info)

        resp_plugins = requests.get(url=url + '/show_plugins')
        plugins = _convert_response(resp_plugins)

        return {
            'server_name': info['server_name'],
            'url': url,
            'plugin_language': info['plugin_language'],
            'plugins': plugins
        }

    def add_plugin_server(self, url):
        """Adds a plug-in server reference and sends an updated plug-in route table to all servers

        Args:
            url(str): URL to the plug-in servers API
        """

        # get info from plug-in server
        server_data = self.get_plugin_server_data(url=url)

        self.plugin_servers.append(server_data)

        # send updated route table to all plugin servers
        for server in self.plugin_servers:
            server_url = server['url']
            resp = self._send_route_table(plugin_server_url=server_url)
            if resp.status_code != 200:
                warnings.warn('Failed to send pluging route table to url: {}'.format(server_url))

    @property
    def plugin_config(self):
        """Returns the plug-in configs of all plug-ins in the system"""

        plugin_config = []

        for server in self.plugin_servers:
            plugin_config += server['plugins']

        return plugin_config

    @property
    def plugin_route_table(self):
        """dict: Returns a dict of plug-in module names and their corresponding server url"""

        plugin_routes = {}
        for server in self.plugin_servers:
            for plugin in server['plugins']:
                if plugin['module'] in plugin_routes.keys():
                    warnings.warn('Plugin name conflict encountered. Got {} for url: {}. But {} is already defined by'
                                  'url: {}. Plug-in will not be added to registry.'.format(plugin['module'],
                                                                                           server['url'],
                                                                                           plugin['module'],
                                                                                           plugin_routes[plugin['module']]))
                else:
                    plugin_routes[plugin['module']] = server['url']

        return plugin_routes

    def run_plugin(self, plugin_name, *params):
        """Starts a plug-in launch request

        Args:
            plugin_name(str): Name of the plug-in to run
            params(list): List of parameters to pass to plug-in
        """

        # get plug-route (address of the server containing the requested plug-in)
        try:
            plugin_url = self.plugin_route_table[plugin_name]
        except KeyError:
            warnings.warn('Plug-in {} was not found in plug-in registry!')
            return

        plugin_data = {'name': plugin_name, 'params': params}

        response = self._request_plugin_run(plugin_server_url=plugin_url, plugin_data=plugin_data)

        result = _convert_response(response)

        return result
