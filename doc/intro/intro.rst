.. _introduction:

Introducion
===========

Framework overview
------------------

The *openmatDB* environment basically consist of three components:

* a data base server exposing a RESTful API (currently couchDB is used)
* one or more plugin-server(s)
* graphical user-interface(s) (desktop and/or browser based)

The plug-in servers form the core of the *openmatDB* environment.
They communicate with each other, the UIs and the data base. They host the plug-ins, that carry out all operations.
Since the communication is based on RESTful APIs and data is exchanged in form of JSON strings plug-in servers for
different programming languages can be created with comparably low effort (as long as JSON handling and HTTP servers
are available for that language).

The UIs basically collect "plug-in run-requests" from the users - i.e. name of the requested plug-in and the parameters
(e.g. record IDs, record attributes for performing a calculation).

The working principle is explained in more detail in :ref:`plugin_run_request`.

The following illustration provides an overview of the system:

.. image:: framework_overview.png

As can be seen in the illustration two classes provide the interfaces of *openmatDB* environment:

* OpenmatEnvironment - provides mainly routing information and easy access to the API calls of the plug-in servers
* CouchClient - provides data base access for plug-ins

The communication principle and the use of the interfaces is illustrated in the following sub-chapters.

Plug-in server initialization procedure
---------------------------------------

When a plug-in server is launched following steps are performed:

1. The plug-in server will query the *register* function of each plug-in on the server
2. The *register* function will then return information on required to work with the plug-in (e.g. data types for its
   input parameters and return values, GUI elements to create to capture these input parameters) - for details see the
   :ref:`plug_ins` chapter.
3. With the return values of all plug-ins the plug-in server create a *plugin_config* dictionary, which it can share
   with the UIs

UI initialization procedure
---------------------------

This section will illustrate the steps of the initialization process of an *OpenmatEnvironment*, when a user interface
is launched.

1. The UI needs to create an *OpenmatEnvironment" object and one or more calls to the *add_plugin_server* method
2. The *add_plugin_server* methods sends some HTTP requests to the plug-in server, which returns information about the
   plug-ins it is hosting (i.e. it returns the *plugin_config* as explained above)
3. The *add_plugin_server* method then updates the *server_list* and the *plugin_route_table* (which is a look-up table
   for plug-in names and their corresponding server - its purpose will be explained in more detail in the
   :ref:`plugin_run_request` chapter.
4. Afterwards the *plugin_route_table* will be send to all plug-in servers in the *server_list* and each server will
   contain a copy of the *plugin_route_table*.

The animation below illustrates the UI initialization process for an example, where two plug-in servers are added to
the environment.

.. image:: interface_init.gif

.. _plugin_run_request:

UI request processing procedure
-------------------------------

When the user has selected a plug-in, entered the required parameter values into the UI elements (e.g. line edits,
spin boxes, combo boxes,...) and launched the plug-in (e.g. by pressing a 'Run plug-in' button) the UI layer sends a
call to the UI's *OpenmatEnvironment* object's *run_plugin* function.

The procedure can be sub-divided into following steps:

1. The *run_plugin* method is called with the plug-in name and parameters provided by the UI layer
2. The *run_plugin* method will first perform a look-up in the *plugin_route_table* to find the server, which is hosting
   the corresponding plug-in
3. It will send an HTTP POST request to the plug-in server with plug-in name and parameter values encoded in the JSON
4. The plug-in server will decode the request and calls the requested plug-in's *run* function passing the parameter
   values provided by the request as the function arguments
5. The plug-in will then execute its code and eventually encounter a request to run another plug-in.
6. In this case the plug-in server's *run_plugin* method is called.
7. The *run_plugin* method will fetch the server address, which is hosting the requested plug-in and dispatch a plug-in
   run-request analogous to the request from the UI
8. A plug-in can also request a couple of data base interactions - i.e. the plug-in will call one of the *CouchClient*'s
   data base request method's
9. The *CouchClient* will then send an HTTP request to the data base and return the (decoded) response to the plug-in
10. The plug-in will eventually do further processing on the data and/or return the information to the */run_plugin*
    API call, which will a response with the plug-ins results to the sender (i.e. a *run_plugin* method of either a
    UI or another plug-in server)
11. When a result is finally returned to the UIs *run_plugin* method, it will decode the information and send it to the
    UI layer, which can render the results accordingly by using the *return_type* definition of the corresponding
    plug-in stored in the *plugin_config* variable (which might tell the UI to render the result e.g. as a chart or a
    table)

The animation below illustrates the UI request processing procedure for a common example.

In this example a user request is received to run "pluginC" (hosted by plug-in server #2). The "pluginC" however relies
on "pluginA", which is stored on plug-in server #1. While "pluginC" does not request any data base interaction "pluginA"
will raise a request for a record stored in the data base.

In a real application "pluginC" could be an implementation of a statistical evaluation, which first calls another
plug-in to fetch the required data for the analysis by forwarding some search parameters provided by the user.

.. image:: user_request.gif


