.. _components:

Software components
===================

The source code of the different components of *openmatDB* are currently provided on 5 different repositories on
*gitlab*.

.. warning::

    *openmatDB* is currently in its early development stage and not suitable for a production environment!
    However A complete test environment (including a desktop and a browser based GUI) can however be downloaded
    - see :ref:`test_env` - for testing/development purposes.

For an overview on the licenses on the different software components see the :ref:`licenses` chapter.
For more details on licensing and copyright information refer to the **NOTICE** and **LICENSE** files
in the corresponding source code repository.

pyopenmatdb
-----------

*pyopenmatdb* provides the main components of the system - i.e. the python classes for the interfaces
*OpenmatEnviroment* and *CouchClient* and the plug-in server.

*openmat-plugins* is released under Apache License Version 2.0.
The source code can be obtained from: https://gitlab.com/thecker/pyopenmatdb

*pyopenmatdb* is written python and depends on following packages:

* python3
* Requests
* Flask
* Flask-CORS

openmat-plugins
---------------

*openmat-plugins* is a collection of plug-ins for *openmatDB*'s python plug-in server.

*openmat-plugins* is released under Apache License Version 2.0.
The source code can be obtained from https://gitlab.com/thecker/openmat-plugins

The plug-ins depend on following python packages:

* python3
* pandas
* xlrd
* matplotlib
* mpld3
* NumPy
* pyopenmatdb

openmat-webui
-------------

The *openmat-webui* repository provides a browser based GUI to the *openmatDB* environment.

.. image::  web-ui.png

*openmat-webui* is free software released itself under the Apache License Version 2.0.
The source code con be obtained from https://gitlab.com/thecker/openmat-webui

It comes bundle with following software components:

* jQuery JavaScript Library v1.12.4, Copyright jQuery Foundation and other contributors
* jQuery UI v1.12.1, Copyright jQuery Foundation and other contributors
* jQuery UI-themes v1.12.1, Copyright jQuery Foundation and other contributors
* Fancytree, v2.34.0, Copyright 2008-2019 Martin Wendt

*openmat-webui* is written python and JavaScript. The JavaScript dependencies are bundled with the software
(see above). For the python part following packages are required:

* python3
* Flask
* Flask-CORS
* pyopenmatdb


openmatGUI
----------

The *openmatGUI* repository provides a GUI, which runs as a local desktop application.

.. image:: desktop-ui.png

*openmatGUI* is free software released under the GNU GENERAL PUBLIC LICENSE Version 3 (GPLv3).

See NOTICE and LICENSE file for details.

*openmatGUI* is written in python and depends on following python packages:

* Python3
* PyQt5
* matplotlib
* pyopenmatdb


openmat-testenv
---------------

The *openmat-testenv* repository contains a collection of configuration files and shell scripts, which automate setting
up and launching a test environment for *openmatDB* - for details see the :ref:`test_env` chapter.

*openmat-testenv* is free software released under the Apache License Version 2.0.

In order to run the shell scripts provided by this repository following programs are required (besides a
UNIX shell):

* Docker CLI
* cURL

Furthermore if you run the shell scripts following pre-built docker images will be
downloaded to your system:

* couchDB:latest, https://hub.docker.com/_/couchdb
* python:3.7, https://hub.docker.com/_/python

The python:3.7 image will be used create two images for the openamtDB environment (openmat-py_plugin:latest and
openmat-webui:latest).
Note, that the python packages (and their dependencies) required by *pyopenmatdb*, *openmat-plugins* and
*openmat-webui* will be downloaded and installed on the images as well (see chapters above).
